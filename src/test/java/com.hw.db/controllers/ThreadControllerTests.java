package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class ThreadControllerTests {
    private User user;
    private Post post;
    private List<Post> posts;
    private Thread thread;
    private ThreadController controller;

    @BeforeEach
    void init() {
        this.user = new User("nickname", "email", "full name", "about");
        this.post = new Post("nickname", Timestamp.from(Instant.now()), "forum", "message", 0, 0, false);
        this.posts = List.of(post);

        this.thread = new Thread("nickname", Timestamp.from(Instant.now()), "forum", "message", "slug", "title", 10);
        this.thread.setId(1);

        this.controller = new ThreadController();
    }

    @Test
    void getById() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(this.thread);
            Assertions.assertEquals(this.thread, controller.CheckIdOrSlug("1"));
        }
    }

    @Test
    void getBySlug() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);
            Assertions.assertEquals(this.thread, controller.CheckIdOrSlug("slug"));
        }
    }

    @Test
    void createPost() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info("nickname")).thenReturn(this.user);

                ResponseEntity response = controller.createPost("slug", this.posts);
                Assertions.assertEquals(this.posts, response.getBody());
                Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
            }
        }
    }

    @Test
    void getPosts() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);
            threadDAOMock.when(() -> ThreadDAO.getPosts(1, 1, 0, "", false)).thenReturn(this.posts);

            ResponseEntity response = controller.Posts("slug", 1, 0, "", false);
            Assertions.assertEquals(this.posts, response.getBody());
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }

    @Test
    void change() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(this.thread);

            ResponseEntity response = controller.change("slug", this.thread);
            Assertions.assertEquals(this.thread, response.getBody());
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }

    @Test
    void getInfo() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);

            ResponseEntity response = controller.info("slug");
            Assertions.assertEquals(this.thread, response.getBody());
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }

    @Test
    void createVote() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(this.thread);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info("nickname")).thenReturn(this.user);

                ResponseEntity response = controller.createVote("slug", new Vote("nickname", 1));
                Assertions.assertEquals((Object)11, ((Thread)response.getBody()).getVotes());
                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            }
        }
    }
}
